﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelController : MonoBehaviour {

    [SerializeField]
    Text triesCountView;

    static int triesCount = 1;

    void Start()
    {
        DontDestroyOnLoad(gameObject);
        triesCountView.text = triesCount.ToString();
    }

    public void RestartAfterSec( float timeout )
    {
        Invoke("Restart", timeout);
    } 

    void Restart()
    {
        triesCount += 1;
        triesCountView.text = triesCount.ToString();
        Application.LoadLevel(Application.loadedLevel);
    }
}
