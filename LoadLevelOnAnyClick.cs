﻿using UnityEngine;
using System;
using System.Collections;

public class LoadLevelOnAnyClick : MonoBehaviour {

    [SerializeField]
    String levelName;
	
	void Update () {
        if (Input.anyKeyDown)
            Application.LoadLevel(levelName);
	
	}
}
