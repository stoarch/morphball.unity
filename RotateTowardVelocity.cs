﻿using UnityEngine;
using System.Collections;

public class RotateTowardVelocity : MonoBehaviour {

    [SerializeField]
    float rotationSpeed = 10F;//degrees per tick

    [SerializeField]
    Rigidbody2D bodyToMove;

    public Rigidbody2D BodyToMove { get { return bodyToMove; } set { bodyToMove = value; } }

	void Update () {
        transform.Rotate(0f, 0f, rotationSpeed * bodyToMove.velocity.x);
	}
}
