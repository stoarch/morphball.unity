﻿using UnityEngine;
using System.Collections;

public class ActivateFormOnEnter : MonoBehaviour {
    [SerializeField]
    PlayerForm giftForm;
    [SerializeField]
    GameObject activationFX;

    void OnTriggerEnter2D(Collider2D target)
    {
        if( target.gameObject.tag == "PlayerForm")
        {
            PlayerController player = target.gameObject.GetComponent<PlayerFormManager>().Player;
            player.ActivateForm(giftForm);
            activationFX.SetActive(true);
        }
    }

}
