﻿using UnityEngine;
using System.Collections;

public class MovePlayerRight : MonoBehaviour {
    [SerializeField]
    Rigidbody2D bodyToMove;
    [SerializeField]
    float speed;

    public Rigidbody2D BodyToMove { get { return bodyToMove; } set { bodyToMove = value; } }

    public float Speed { get { return speed; } set { speed = value; } }

	void Update () {
        bodyToMove.AddForce(new Vector2(speed, 0), ForceMode2D.Force);
	}
}
