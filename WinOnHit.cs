﻿using UnityEngine;
using System.Collections;

public class WinOnHit : MonoBehaviour {
    [SerializeField]
    GameObject winPanel;
    [SerializeField]
    GameObject winFX;

    void OnTriggerEnter2D( Collider2D target )
    {
        if(target.gameObject.tag == "Player" )
        {
            Destroy(target.gameObject);
            winPanel.SetActive(true);
            winFX.SetActive(true);
        }

        if(target.gameObject.tag == "PlayerForm")
        {
            Destroy(target.gameObject.GetComponent<PlayerFormManager>().Player.gameObject);
            winPanel.SetActive(true);
            winFX.SetActive(true);
        }
    }
}
