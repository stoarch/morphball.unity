﻿using UnityEngine;
using System.Collections;

public class DeathAfterTime : MonoBehaviour {

    [SerializeField]
    float timeOut = 1;//sec

    float startTime = 0;

    void Start()
    {
        startTime = Time.time;
    }

	void Update () {

        if (Time.time - startTime > timeOut)
            Destroy(gameObject);
	}
}
