﻿using UnityEngine;
using System.Collections;

public class KillPlayerNotInForm : MonoBehaviour {
    [SerializeField]
    PlayerForm triggerForm;

    void OnTriggerEnter2D( Collider2D target )
    {
        if( target.gameObject.tag == "Player")
        {
            var player = target.gameObject.GetComponent<PlayerController>();
            if (!player)
                return;

            if( player.ActiveForm != triggerForm)
            {
                player.Kill();
            }
            return;
        }

        if( target.gameObject.tag == "PlayerForm")
        {
            var playerForm = target.gameObject.GetComponent<PlayerFormManager>();
            if (!playerForm)
                return;

            if( playerForm.Player.ActiveForm != triggerForm)
            {
                playerForm.Player.Kill();
            }
        }
    }
}
