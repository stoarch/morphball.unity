﻿using UnityEngine;
using System.Collections;

public class PlayerFormManager : MonoBehaviour {
    [SerializeField]
    PlayerController player;
    [SerializeField]
    PlayerForm form;

    public PlayerForm Form { get { return form; } }
    public PlayerController Player { get { return player; } }

}
