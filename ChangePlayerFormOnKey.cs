﻿using UnityEngine;
using System.Collections;
using System;

public class ChangePlayerFormOnKey : MonoBehaviour {

    [Serializable]
    public class PlayerFormKey
    {
        [SerializeField]
        KeyCode key;
        [SerializeField]
        PlayerForm form;

        public KeyCode Key { get { return key; } }
        public PlayerForm Form { get { return form; } }
    }

    [SerializeField]
    PlayerController player;
    [SerializeField]
    PlayerFormKey[] keyForms;



	void Start () {
	
	}
	
	void Update () {
        if( Input.anyKey )
        {
            for (int i = 0; i < keyForms.Length; i++)
            {
               if(Input.GetKey(keyForms[i].Key))
                {
                    player.ChangeForm(keyForms[i].Form);
                }
            }
        }
	
	}
}
