﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.SceneManagement;
using System.Runtime.Serialization;

public enum PlayerForm
{
    Unknown,
    Normal,
    Spike,
    Bar
}


public class PlayerController : MonoBehaviour {

    [Serializable]
    public class PlayerFormView 
    {
        [SerializeField]
        PlayerForm viewForm;
        [SerializeField]
        GameObject view;
        [SerializeField]
        float speed = 1.0f;
        [SerializeField]
        Collider2D collider;
        [SerializeField]
        private bool active = true;

        public PlayerForm Form { get { return viewForm;  } }
        public GameObject View { get { return view; } }
        public float Speed { get { return speed;  } }
        public Collider2D Collider { get { return collider; } }

        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

    }

    Dictionary<PlayerForm, PlayerFormView> formToView;

    internal void ActivateForm(PlayerForm giftForm)
    {
        var view = formToView[giftForm];
        if( view != null )
        {
            view.Active = true;
        }
    }

    [SerializeField]
    SmoothCamera2D cameraFollow;

    [SerializeField]
    MovePlayerRight movement;

    [SerializeField]
    GameObject[] debris;

    [SerializeField]
    LevelController levelController;

    [SerializeField]
    PlayerFormView[] formViews;
    [SerializeField]
    private GameObject activeView;

    public PlayerForm ActiveForm { get; set; }
    public GameObject ActiveView { get { return activeView;  } set { activeView = value; } }

    internal void ChangeForm(PlayerForm form)
    {
        bool foundView = false;
        for (int i = 0; i < formViews.Length; i++)
        {
            if ((formViews[i].Form == form) && (formViews[i].Active))
            {
                foundView = true;
                formViews[i].View.SetActive(true);
                ActiveForm = formViews[i].Form;
                formViews[i].Collider.enabled = true;
                formViews[i].View.transform.position = ActiveView.transform.position;
                ActiveView = formViews[i].View;
                movement.Speed = formViews[i].Speed;
                movement.BodyToMove = formViews[i].View.GetComponent<Rigidbody2D>();
                if( cameraFollow )
                    cameraFollow.target = ActiveView.transform;
            }
        }

        if( foundView )
            for (int i = 0; i < formViews.Length; i++)
            {
                if( formViews[i].View != ActiveView )
                {
                    formViews[i].View.SetActive(false);
                    formViews[i].Collider.enabled = false;
                }
            }
    }

    void Start()
    {
        ActiveForm = PlayerForm.Normal;
        formToView = new Dictionary<PlayerForm, PlayerFormView>();
        for (int i = 0; i < formViews.Length; i++)
        {
            formToView[formViews[i].Form] = formViews[i];
        }
    }


    public void Kill()
    {
        SpawnDebris();
        levelController.RestartAfterSec(3.0f);
        Destroy(gameObject);
    }


    private void SpawnDebris()
    {
        if (debris.Length == 0)
            return;

        for (int i = 0; i < debris.Length; i++)
        {
            var bit = (GameObject)Instantiate(debris[i], ActiveView.transform.position, Quaternion.identity);
            var rigidBody = bit.GetComponent<Rigidbody2D>();
            if (rigidBody != null)
            {
                rigidBody.AddForce(Vector3.right * (UnityEngine.Random.Range (-50, 50)));
                rigidBody.AddForce(Vector3.up * UnityEngine.Random.Range(50, 100));
            }
        }
    }
}
